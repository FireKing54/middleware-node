import crypto from 'crypto';

const RAW_HASH_LEN = 64;
const SALT_LEN = 16;
const HASH_LEN = RAW_HASH_LEN + SALT_LEN;

/**
 * Function that returns the hash of given password and salt
 * @param {string} password password to hash
 * @param {byte[]} salt salt to hash
 * @returns {byte[]} hashed password
 */
async function hash(password: string, salt: Buffer): Promise<Buffer>{
    const key: Buffer = crypto.scryptSync(password, salt, RAW_HASH_LEN);
    return key;
}

/**
 * Function that returns a salted hash of a password
 * @param {string} password password to hash
 * @returns {byte[]} a salted hash
 */
async function getSaltedHash(password: string): Promise<Buffer> {
    const salt = await getSalt();
    const simpleHash = await hash(password, salt);
    const saltedHash = Buffer.concat([simpleHash, salt], HASH_LEN);
    return saltedHash;
}

/**
 * Function that verifies if a hash and a password matches
 * @param {byte[]} completeHash Complete hash (hash and salt)
 * @param {string} clearPassword Clear password
 * @returns {boolean} true if hash and password match.
 */
async function verify(completeHash: Buffer, clearPassword: string): Promise<boolean> {
    if (completeHash.length !== SALT_LEN + RAW_HASH_LEN) {
      throw new Error("Invalid hash length");
    }
    const simpleHash = completeHash.slice(0, RAW_HASH_LEN);
    const salt = completeHash.slice(RAW_HASH_LEN, HASH_LEN);

    const givenHash: Buffer = await hash(clearPassword, salt);
  
    const isMatch = givenHash.equals(simpleHash);
    
    return isMatch;
}

/**
 * Function that returns a 16 bytes length salt
 * @returns {byte[]} the salt
 */
async function getSalt(): Promise<Buffer> {
    return crypto.randomBytes(SALT_LEN);
}

export {
    hash,
    getSalt,
    verify,
    getSaltedHash
}