import { request_full, request_one_element, request_one_object } from "../../request/request";

async function start() {
    console.log("rh")
    const res = await request_full("SELECT * FROM Lands");
    console.log(res);
    console.log("".padStart(75, "-"));
    const res2 = await request_one_object("SELECT * FROM Lands");
    console.log(res2);
    console.log("".padStart(75, "-"));
    const res3 = await request_one_element("SELECT * FROM Lands");
    console.log(res3);

    for(let i = 0 ; i < 1_000 ; i++) {
        await request_full("SELECT * FROM Lands");
    }
}

console.log("start")
start().then(() => console.log("finished"));