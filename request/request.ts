require('dotenv').config();
import { Pool } from "pg";

type Rows = Array<Row>;
type Logger = (message: any) => void;

interface Row {
    [key: string]: any
}

const pool = new Pool({
    user: process.env.DB_USER,
    host: process.env.DB_HOST,
    database: process.env.DB_NAME,
    password: process.env.DB_PASSWORD,
    port: parseInt(process.env.DB_PORT as string),
    max: 1
});

let logger: Logger = (mess: any) => {};

/**
 * Sets le log function. Default is no log function
 * @param _logger 
 */
function setLogger(_logger: Logger) {
    logger("Changed Log function");
    logger = _logger;
}

/**
 * Request the specified DB and keep rows
 * @param {string} query postgreSQL query
 * @param args arguments passed to the query
 * @returns {Promise<Rows>} result of the query
 */
async function request_full(query: string, ...args: Array<any>): Promise<Rows> {
    const res: Rows = await execute(query, args);
    return res;
}

/**
 * Internal function that executes and logs a bit of information
 * @param {string} query postgreSQL query
 * @param args arguments passed to the query
 */
async function execute(query: string, args: Array<any>): Promise<Rows> {
    // Whether it is passed as an array or as several variables
    if (args[0] instanceof Array) {
        args = args[0];
    }
    const start = Date.now();
    const res = await pool.query(query, args);
    const duration = Date.now() - start;
    logger(`executed query ${{
        query,
        duration,
        rows: res.rowCount
    }}`, );
    return res.rows;
}

/**
 * Used for inserts and updates ONLY. Returns the row inserted or updated.
 * The query MUST NOT contain `RETURNING` statement, since it is already added in this function
 * @param {string} query postgreSQL query
 * @param args arguments passed to the query
 */
async function request(query: string, ...args: Array<any>): Promise<any> {
    return await execute(`${query} RETURNING *`, args);
}

/**
 * Function that returns the first Row object of a query. Useful when you know that just one row is getting out
 * @param {string} query postgreSQL query
 * @param args arguments passed to the query
 * @returns the first single row if there's one, `undefined` otherwise
 */
async function request_one_object(query: string, ...args: Array<any>): Promise<Row | undefined> {
    const [first] = await execute(query, args);
    return first;
}

/**
 * Function that returns one single element. Useful for getting directly a `number` for `COUNT(*)` queries
 * @param {string} query postgreSQL query
 * @param args arguments passed to the query
 * @returns the first element of the first row if exists, `undefined` otherwise
 */
async function request_one_element(query: string, ...args: Array<any>): Promise<any> {
    const [first] = await execute(query, args);
    // If there is no row
    if (first === undefined) return undefined;
    const [property] = Object.getOwnPropertyNames(first);
    return first[property];
}

export {
    request_full,
    request_one_element,
    request_one_object,
    request,
    setLogger
};

export type {
    Rows,
    Row, 
    Logger
}