const colors = {
    black: `\x1b[30m`,
    red: `\x1b[31m`,
    green: `\x1b[32m`,
    yellow: `\x1b[33m`,
    blue: `\x1b[34m`,
    magenta: `\x1b[35m`,
    cyan: `\x1b[36m`,
    white: `\x1b[37m`,
};

type Colored = "white" | "black" | "red" | "green" | "yellow" | "blue" | "magenta" | "cyan" | "white";

/**
 * Log a message with a color
 * @param {string} message Message to print
 * @param {Colored} color color to display (black, red, green, yellow, blue, magenta, cyan, white)
 */
function logcolor(message: string, color: Colored = "white") {
    console.log(`${(colors[color]) ? colors[color] : colors.white}%s${reset()}`, message);
}

function reset() {
    return "\x1b[0m"
}

/**
 * Print a warning
 * @param {string} message message to print
 */
function logwarning(message: string) {
    logcolor(`${getExactDate()} WARNING : ${message}`, "yellow");
}

/**
 * Function that returns the current date in HHMMSS - DDMMYYYY format
 */
function getExactDate() {
    const date = new Date();
    return `[${date.toLocaleDateString("fr-FR")} - ${date.toLocaleTimeString("fr-FR")}]`
}

/**
 * Print an error
 * @param {string} message message to print
 */
function logerror(message: string) {
    logcolor(`${getExactDate()} ERROR : ${message}`, "red");
}

/**
 * Print a success message
 * @param {string} message message to print
 */
function logsuccess(message: string) {
    logcolor(`${getExactDate()} SUCCESS : ${message}`, "green");
}

export {
    logcolor,
    logwarning,
    logerror,
    logsuccess
}