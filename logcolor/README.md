# logcolor

This short module can print colored outputs.

## How it works

`logcolor(message, color)`
prints *message* in the specified *color*.

Supported colors :
* black
* blue
* cyan
* green
* magenta
* red
* white
* yellow

If ever the color is **not specified**, the message is printed in **white**.

There are also 2 other functions :
`logwarning(message)`
`logerror(message)`

All of them are **synchronous**